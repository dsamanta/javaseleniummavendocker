#Selenoid Run with Local Host :
##################################
docker-compose run --service-ports --use-aliases -d selenoid-ui

In this case the WebDriver Address will be
"http://localhost:4444/wd/hub"


#Selenoid Run with without Host :
#####################################
docker-compose run --use-aliases -d selenoid

WebDriver Address#
"http://selenoid:4444/wd/hub"


Use the Make file to Run Test for Different browsers and Environment
#####################################################################
First You should run
make up
'This will spin up the docker with selenoid browsers'

Second :
If you would like to run test in Browser : Chrome in Test Environment
Note :
3 Environments are created for ease of use
dev
test
staging

Refer to config/environment for properties file

e.g. :
make run/selenoid-chrome environment=dev
or
make run/selenoid-chrome environment=test
or
make run/selenoid-chrome environment=staging

Third:
Once you are done with your testing. You should kill the docker to release memory.

e.g.
make down