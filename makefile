up:
	docker-compose run --service-ports --use-aliases -d selenoid-ui

down:
	docker-compose down

clean:
	mvn clean

run/selenoid-chrome:
	mvn clean test -Dbrowser='chrome' -Denv=$(environment)


run/selenoid-firefox:
	mvn clean test -Dbrowser='firefox' -Denv=$(environment)


run/selenoid-safari:
	mvn clean test -Dbrowser='safari' -Denv=$(environment)



