package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class homePage {
    RemoteWebDriver Driver;
    By inputBox = By.cssSelector("[data-testid='name-input']");

    public void getDriver(RemoteWebDriver driver){
        Driver = driver;
    }

    public void getUrl(String url){
        Driver.get(url);
    }

    public void typeText(String textInput) throws InterruptedException {
        System.out.println("I am inside Type Text");
        this.implicitWait(inputBox);
        Driver.findElement(inputBox).clear();
        Driver.findElement(inputBox).sendKeys(textInput);
        Thread.sleep(20000);
    }

    public void implicitWait(By element){
        System.out.println("I am inside Wait ");
        WebDriverWait wait = new WebDriverWait(Driver,10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(element));
    }
}
