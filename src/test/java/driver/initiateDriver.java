package driver;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URI;

public class initiateDriver {


    RemoteWebDriver driver;
    String selenoidUrl = "http://localhost:4444/wd/hub";

    public RemoteWebDriver getDriver(String browser) throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();

        System.out.println("Browser Name is :" +browser);

        if(browser.equals("chrome")) {
            capabilities.setCapability("browserName", "chrome");
            capabilities.setCapability("browserVersion", "latest");
        } else if (browser.equals("firefox")) {
            System.out.println("Inside FF ");
            capabilities.setCapability("browserName", ":firefox");
            capabilities.setCapability("browserVersion", "93.0");
        } else {
            capabilities.setCapability("browserName", "safari");
            capabilities.setCapability("browserVersion", "14.0");
        }

        capabilities.setCapability("enableVNC", true);
        capabilities.setCapability("enableVideo", true);

        System.out.println("Setting Driver ");
        driver = new RemoteWebDriver(
                URI.create(selenoidUrl).toURL(),
                capabilities
        );

        return driver;

        //driver.get("https://devexpress.github.io/testcafe/example/");


    }
}
