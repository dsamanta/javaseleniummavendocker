package specs;

import driver.initiateDriver;
import helper.readEnvironment;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.homePage;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;

public class loginSpec extends initiateDriver {
    RemoteWebDriver Driver;
    homePage HomePage = new homePage();
    readEnvironment readEnvDetails = new readEnvironment();

    @BeforeTest
    public void testMethod() throws MalformedURLException {
        String browser = System.getProperty("browser");
        Driver = getDriver(browser);
        HomePage.getDriver(Driver);
    }


    @Test
    public void mainTest() throws InterruptedException, FileNotFoundException {
        String env = System.getProperty("env");

        HomePage.getUrl(readEnvDetails.readEnv(env,"url"));
        HomePage.typeText("My Name is Debashish Samanta");
    }

    @AfterTest
    public void endOfTest(){
        Driver.quit();
    }

}
