package helper;

import javax.imageio.IIOException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class readEnvironment {
    String propertyValue;
    InputStream inputStream;

    public String readEnv(String env, String value) throws FileNotFoundException {
        String fileName = "./config/environment/env_" + env + ".properties";

        InputStream input = new FileInputStream(fileName);
        Properties prop = new Properties();
        try {
            prop.load(input);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return propertyValue = prop.getProperty(value);
    }
}
